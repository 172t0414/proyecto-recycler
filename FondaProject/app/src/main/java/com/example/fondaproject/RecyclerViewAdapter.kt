package com.example.fondaproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

    private val itemTitles = arrayOf("Molletes", "Huevos rancheros", "Chilaquiles rojos", "Picaditas veracruzanas")
    private val itemDes = arrayOf("Mollete con frijoles, queso manchego y pico de gallo (1 pza)",
        "Huevos fritos sobre tortillas de maíz con una salsa y frijoles refrito",
        "Totopos de maiz concinados en una salsa roja picante",
        "Torilla de maiz con salsa encima con queso fresco y pollo. (3 pzas.)")
    private val itemImage = intArrayOf(
            R.drawable.ima_mollete,
            R.drawable.huevos,
            R.drawable.chilaquiles,
            R.drawable.picaditas
    )

    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        var image: ImageView
        var titles: TextView
        var des: TextView
        var rat: RatingBar

        init {
            image = itemView.findViewById(R.id.img_item)
            titles = itemView.findViewById(R.id.txtTitle)
            des = itemView.findViewById(R.id.txtDescription)
            rat = itemView.findViewById(R.id.rating)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.cardviewdesayunos,parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titles.text = itemTitles [position]
        holder.des.text = itemDes[position]
        holder.image.setImageResource(itemImage [position])
        holder.rat.numStars = 3

        holder.itemView.setOnClickListener{v: View ->
            Toast.makeText(v.context, "Click en el elemento", Toast.LENGTH_SHORT).show()

        }

    }

    override fun getItemCount(): Int {
        return itemTitles.size
    }

}