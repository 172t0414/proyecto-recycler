package com.example.fondaproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter2 : RecyclerView.Adapter<RecyclerViewAdapter2.ViewHolder>() {

    private val itemTitles =
        arrayOf("Tacos de bistec", "Tostadas", "Flautas de pollo", "Carne tampiqueña")
    private val itemDes = arrayOf("Tacos tradicionales de bistec con tortilla de maz (5 tacos la orden)",
        "Torilla frita de maiz con frijoles, queso, pollo, crema y aguacate (1 pza).",
        "Torilla de maiz frita rellanas de pollo con lechuga, queso y crema por encima. (3 pzas)",
        "Arranchera acomprañada de frijoles, guacamole, totopos y ensalada.")
    private val itemImage = intArrayOf(
        R.drawable.img_tacos,
        R.drawable.img_tostadas,
        R.drawable.img_flautas,
        R.drawable.img_carne
    )

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var titles: TextView
        var des: TextView
        var rat: RatingBar

        init {
            image = itemView.findViewById(R.id.img_item)
            titles = itemView.findViewById(R.id.txtTitle)
            des = itemView.findViewById(R.id.txtDescription)
            rat = itemView.findViewById(R.id.rating)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.cardviewdesayunos, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titles.text = itemTitles[position]
        holder.des.text = itemDes[position].toString()
        holder.image.setImageResource(itemImage[position])
        holder.rat.numStars = 3

        holder.itemView.setOnClickListener { v: View ->
            Toast.makeText(v.context, "Click en el elemento", Toast.LENGTH_SHORT).show()

        }

    }

    override fun getItemCount(): Int {
        return itemTitles.size
    }

}