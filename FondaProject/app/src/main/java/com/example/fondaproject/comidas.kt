package com.example.fondaproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class comidas : AppCompatActivity() {
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerViewAdapter2.ViewHolder>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comidas)

        val lista: RecyclerView
        lista = findViewById(R.id.listComidas)

        layoutManager = LinearLayoutManager( this)
        lista.layoutManager = layoutManager

        adapter = RecyclerViewAdapter2 ()
        lista.adapter = adapter
    }
}