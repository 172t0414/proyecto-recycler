package com.example.fondaproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var boton: Button
        var btRe: Button

        boton = findViewById(R.id.btnI)
        btRe = findViewById(R.id.btnRegistrar)

        boton.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, Menu::class.java)
            startActivity(intent)

        })

        btRe.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, Registrar::class.java)
            startActivity(intent)

        })





    }
}