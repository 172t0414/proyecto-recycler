package com.example.fondaproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class Menu : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val btDe:Button
        val btCo: Button
        val btBe: Button
        val btRe: Button

        btDe = findViewById(R.id.btnDesayunos)
        btCo = findViewById(R.id.btnComidas)
        btBe = findViewById(R.id.btnBebidas)


        btDe.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, desayunos::class.java)
            startActivity(intent)

        })

        btCo.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, comidas::class.java)
            startActivity(intent)

        })

        btBe.setOnClickListener(View.OnClickListener {
            val intent = Intent(applicationContext, bebidas::class.java)
            startActivity(intent)

        })



    }
}