package com.example.fondaproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class desayunos : AppCompatActivity() {

    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_desayunos)

        val lista: RecyclerView
        lista = findViewById(R.id.listDesayunos)

        layoutManager = LinearLayoutManager( this)
        lista.layoutManager = layoutManager

        adapter = RecyclerViewAdapter ()
        lista.adapter = adapter
    }
}