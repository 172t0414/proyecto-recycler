package com.example.fondaproject

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter3 : RecyclerView.Adapter<RecyclerViewAdapter3.ViewHolder>() {

    private val itemTitles =
        arrayOf("Café lechero", "Agua de horchata", "Agua de jamaica", "Limonada")
    private val itemDes = arrayOf("Café con leche entera o deslactosada", "Agua fresca de horchata", "Agua fresca de jamaica", "Limonada tradicional americana")
    private val itemImage = intArrayOf(
        R.drawable.img_cafe,
        R.drawable.img_horchata,
        R.drawable.img_jamaica,
        R.drawable.img_limonada
    )

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView
        var titles: TextView
        var des: TextView
        var rat: RatingBar

        init {
            image = itemView.findViewById(R.id.img_item)
            titles = itemView.findViewById(R.id.txtTitle)
            des = itemView.findViewById(R.id.txtDescription)
            rat = itemView.findViewById(R.id.rating)
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.cardviewdesayunos, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titles.text = itemTitles[position]
        holder.des.text = itemDes[position].toString()
        holder.image.setImageResource(itemImage[position])
        holder.rat.numStars = 3

        holder.itemView.setOnClickListener { v: View ->
            Toast.makeText(v.context, "Click en el elemento", Toast.LENGTH_SHORT).show()

        }

    }

    override fun getItemCount(): Int {
        return itemTitles.size
    }

}